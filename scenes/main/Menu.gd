extends Panel

var timer_on_start = false
var second = 60
var sound = false
var console = preload("res://scenes/console/console.tscn")
var list_sound = [
	preload("res://assets/sound/danua.wav"),
	preload("res://assets/sound/end.wav"),
	preload("res://assets/sound/start.wav"),
	preload("res://assets/sound/stop.wav"),
	preload("res://assets/sound/scream.wav")
]

var meme = false
var obj

func _ready():
	$VBoxContainer/Button2.disabled = true

func _on_HSlider_value_changed(value):
	second = value*60
	$CenterContainer/Label.text = str(value) + ":00"

func _on_Timer_timeout():
	$Timer.stop()
	second = 60
	timer_on_start = false
	if sound and !$AudioStreamPlayer.is_playing() and !meme:
		$AudioStreamPlayer.stream = list_sound[1]
		$AudioStreamPlayer.play()
	elif sound and !$AudioStreamPlayer.is_playing() and meme:
		$AudioStreamPlayer.stream = list_sound[4]
		$AudioStreamPlayer.play()
	
	$CenterContainer/Label.text = "1:00"
	$HSlider.editable = true
	$Start.texture_normal = load("res://assets/buttons/start.png")
	$Start.texture_hover = load("res://assets/buttons/start_hover.png")
	$Start.texture_pressed = load("res://assets/buttons/start_pressed.png")

func _on_Start_pressed():
	if not timer_on_start:
		timer_on_start = true
		$Start.texture_normal = load("res://assets/buttons/stop.png")
		$Start.texture_hover = load("res://assets/buttons/stop_hover.png")
		$Start.texture_pressed = load("res://assets/buttons/stop_pressed.png")
		$HSlider.editable = false
		$Timer.wait_time = second
		$HSlider.value = 1
		$Timer.start()
		if sound and !$AudioStreamPlayer.is_playing():
			$AudioStreamPlayer.stream = list_sound[2]
			$AudioStreamPlayer.play()
	else:
		$HSlider.editable = true
		$Timer.stop()
		timer_on_start = false
		$Start.texture_normal = load("res://assets/buttons/start.png")
		$Start.texture_hover = load("res://assets/buttons/start_hover.png")
		$Start.texture_pressed = load("res://assets/buttons/start_pressed.png")
		$CenterContainer/Label.text = "1:00"
		second = 60
		if sound and !$AudioStreamPlayer.is_playing():
			$AudioStreamPlayer.stream = list_sound[3]
			$AudioStreamPlayer.play()

func _on_Button_pressed():
	$background.texture = load("res://assets/gray/сер фон.png")
	$table.texture = load("res://assets/gray/един серый.png")
	$table.scale = Vector2(0.14, 0.14)
	meme = false
	$VBoxContainer/Button.disabled = true
	$VBoxContainer/Button2.disabled = false
	$VBoxContainer/Button3.disabled = false


func _on_Button2_pressed():
	$background.texture = load("res://assets/malinka/malinka.png")
	$table.texture = load("res://assets/malinka/malinka_table.png")
	$table.scale = Vector2(0.14, 0.14)
	meme = false
	$VBoxContainer/Button.disabled = false
	$VBoxContainer/Button2.disabled = true
	$VBoxContainer/Button3.disabled = false


func _on_Button3_pressed():
	$background.texture = load("res://assets/white/бел фон.png")
	$table.texture = load("res://assets/white/един бел.png")
	$table.scale = Vector2(0.14, 0.14)
	meme = false
	$VBoxContainer/Button.disabled = false
	$VBoxContainer/Button2.disabled = false
	$VBoxContainer/Button3.disabled = true


func _on_Button4_pressed():
	if sound:
		sound = false
		$VBoxContainer/Button4.texture_normal = load("res://assets/buttons/sound_on.png")
	else:
		sound = true
		$VBoxContainer/Button4.texture_normal = load("res://assets/buttons/sound_off.png")

func _process(delta):
	if timer_on_start:
		if int(round($Timer.get_time_left()))%60 < 10:
			$CenterContainer/Label.text = str(int(round($Timer.get_time_left()))/60) + ":0" + str(int(round($Timer.get_time_left()))%60)
		else:
			$CenterContainer/Label.text = str(int(round($Timer.get_time_left()))/60) + ":" + str(int(round($Timer.get_time_left()))%60)
	if Input.is_action_just_pressed("ui_f1"):
		if obj in get_children(): 
			obj.queue_free()
		else:
			obj = console.instance()
			self.add_child(obj)
